
	let getCube = enterNum => {
		const result = enterNum ** 3
		return `The cube of ${enterNum} is ${result}`
	};

	console.log(getCube(2));


	let address = [`Cambridge Village`, `Brgy San Andres`, `Cainta Rizal`, `Philippines`];

	let [village, barangay, city, country] = address;

	console.log(`I live at ${village} ${barangay} ${city} ${country}`);

	let animal = {
		name: `Lolong`,
		kind: `Saltwater crocodile`,
		weight: 1075,
		measurementfeet: 20,
		measurementInch: 3
	}

	let {name, kind, weight, measurementfeet,measurementInch} = animal;

	console.log(`${name} was a ${kind}. He weighed at ${weight} kgs with a measurement of ${measurementfeet} ft ${measurementInch} in`);


	let arrNumbers = [1,2,3,4,5];

	arrNumbers.forEach(num => console.log(num));

    let total = arrNumbers.reduce((curr, next) => curr + next);
    console.log(total);

    class Dog {
    	constructor (name, age, breed){
    	this.name = name;
    	this.age = age;
    	this.breed = breed;
    	}
    }

    let dog1 = new Dog(`Frankie`, 5, `Miniature Dachshund`);
    console.log(dog1);



